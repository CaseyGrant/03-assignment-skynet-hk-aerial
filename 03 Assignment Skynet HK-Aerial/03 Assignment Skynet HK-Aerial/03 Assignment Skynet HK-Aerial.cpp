#include <iostream>
#include <cstdlib>

using namespace std;
int searchGridHighNumber = 64;
int searchGridLowNumber = 1;
int targetLocationPrediction;
int Location;

int main()
{
    cout << "Generating enemy location... \n";
	Location = rand() % 64 + 1;
	cout << "The location is " << Location << "\n";
	cout << "Starting drone search... \n";
    do
    {
		if (Location != 64)
		{
			targetLocationPrediction = ((searchGridHighNumber - searchGridLowNumber) / 2) + searchGridLowNumber;
		}
		else
		{
			targetLocationPrediction = 64;
		}
    	
		cout << targetLocationPrediction << "\n";

    	if (Location > targetLocationPrediction)
		{
			searchGridLowNumber = targetLocationPrediction;
		}
		else if (Location < targetLocationPrediction)
		{
			searchGridHighNumber = targetLocationPrediction;
		}
		else 
		{
			cout << "Enemy located \n";
		}
    	
    }
    while (targetLocationPrediction != Location);
	searchGridHighNumber = 64;
	searchGridLowNumber = 1;
	system("pause");
	main();
}
