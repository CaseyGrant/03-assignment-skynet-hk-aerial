#include <iostream>
#include <cstdlib>
#include <time.h>
#include <string>

using namespace std;
int searchGridHighNumber = 64;
int searchGridLowNumber = 1;
int targetLocationPrediction;
int Location;
float incrementalGuess;
int n;
int randomGuess;
int highLow;
int oneByOne;
int random;
string playerGuess;
int playerInput;
int exceptionCheck;
string playerInputString;
void isMultipleof5();

int main()
{
    cout << "Generating enemy location 1-64... \n";
	srand(time(0)); // generates a unique random seed
	Location = rand() % 64 + 1; // sets the location to a random number between 1 and 64
	cout << "Time to try to find the enemy! \n";
	cout << "Guess the location \n";
    do // starts a do/while loop
    {
		playerInput++; // increments the player input number
		cin >> playerGuess; // gets the player input
		try // goes to catch if any errors occur in the try block
		{
			exceptionCheck = stoi(playerGuess); // makes the input into a number
			if (exceptionCheck != Location) // checks if the input was wrong
			{
				cout << "Try again... \n";
				if (exceptionCheck < Location) // checks if the guess was lower than the target
				{
					cout << "The location is higher... \n";
				}
				if (exceptionCheck > Location)// checks if the guess was higher than the target
				{
					cout << "The location is lower... \n";
				}
			}
			if (exceptionCheck == Location) // checks if the guess was the target
			{
				cout << "Enemy located in " << playerInput << " attempts. \n";
			}
		}
		catch (...) // runs if there is an error
		{
			cout << "Input must be a number between 1-64 \n";
		}
	    
		
    }
    while (exceptionCheck != Location); // loops until the player gets it right
	
	cout << "The location is " << Location << "\n";
	cout << "Starting drone search type High-Low... \n";
    do
    {
		highLow++; // increments the ai's attempts
		if (Location != 64)
		{
			targetLocationPrediction = ((searchGridHighNumber - searchGridLowNumber) / 2) + searchGridLowNumber; // halves possible answers to make a guess
		}
		else
		{
			targetLocationPrediction = 64; // makes the prediction the location since otherwise it would crash
		}
    	
		cout << targetLocationPrediction << "\n";

    	if (Location > targetLocationPrediction)
		{
			searchGridLowNumber = targetLocationPrediction; // updates the low
		}
		else if (Location < targetLocationPrediction)
		{
			searchGridHighNumber = targetLocationPrediction; // updates the high
		}
		else 
		{
			cout << "Enemy located in " << highLow << " attempts. \n";
		}
		n++; // increments an int by 1
		isMultipleof5(); // checks if the int is a multiple of five
    }
    while (targetLocationPrediction != Location);

	n = 0; // resets n
	
	do {
		oneByOne++; // increments the ai's attempts
		if (incrementalGuess < Location)
		{
			incrementalGuess++; // increments the ai's guess by 1
			cout << incrementalGuess << ", ";
		}
		if (incrementalGuess == Location)
		{
			cout << "\nEnemy located in " << oneByOne << " attempts. \n";
		}

		n++; // increments an int by 1
		isMultipleof5(); // checks if the int is a multiple of five
		
	}
	while (incrementalGuess != Location);

	cout << "Starting drone search type Random... \n";
	n = 0; // resets n
	
    do
    {
		random++; // increments the ai's attempts
	    if (randomGuess != Location)
	    {
			randomGuess = rand() % 64 + 1; // guesses a random number
			cout << randomGuess << ", ";
	    }
	    if (randomGuess == Location)
	    {
			cout << "\nEnemy located in " << random << " attempts. \n";
	    }
		
		n++; // increments an int by 1
		isMultipleof5(); // checks if the int is a multiple of five
    }
    while (randomGuess != Location);

	n = 0; // resets n
	
	searchGridHighNumber = 64; // resets high
	searchGridLowNumber = 1; // resets low
	incrementalGuess = 0; // resets guess attempts
	highLow = 0; // resets high low attempts
	oneByOne = 0; // resets one by one attempts
	random = 0; // resets random attempts
	system("pause");
	do {
		cout << "Do you want to play again? \n";
		
		cin >> playerInputString; // gets the user input
		
		if (playerInputString == "yes")
		{
			main(); // restarts application
		}
		if (playerInputString == "no")
		{
			cout << "okay bye..."; // exits application
		}
		else
		{
			cout << "Input not a valid answer try yes or no! \n";
		}
	} while (true);
}

void isMultipleof5()
{
	while (n > 0)
		n = n - 5; // checks if its a multiple of 5

	if (n == 0)
		cout << "\n"; // ends the line
}